<?php

namespace Perso\MyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Perso\MyBundle\Entity\Device;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

use Symfony\Component\Form\Extension\Core\Type\DateType;

use Symfony\Component\Form\Extension\Core\Type\FormType;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use Symfony\Component\Form\Extension\Core\Type\TextType;

class DeviceController extends Controller
{
    /**
     * @Route("/index")
     */
    public function indexAction()
    {
        



        //$form = $this->createForm(DeviceType::class, $device);
        $em = $this->getDoctrine()->getManager();
        $listAllDevices= $em->getRepository('PersoMyBundle:Device')->findAll();
        return $this->render('PersoMyBundle:Device:index.html.twig', array(
            // ...
            
            'alldevices'=>$listAllDevices

        ));
    }

    /**
     * @Route("/add")
     */
    public function addAction(Request $request)
    {
        $device = new Device();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $device);

        $formBuilder

          ->add('name',      TextType::class)

          ->add('guid',     TextType::class)

          ->add('protocol',   TextType::class)

          
        ;

        $form = $formBuilder->getForm();
        $em=$this->getDoctrine()->getManager();
        $em->persist($device);

        if($request->isMethod('POST')){
            $data=$form->getData();
            $device->setName($data['name']);
            
            $device->setGuid($data['guid']);
            $device->setProtocol($data['protocol']);
            $request->getSession()->getFlashBag()->add('tag','les actions ont été save');
            return $this->redirectToRoute('perso_my_bridges');
        }
        return $this->render('PersoMyBundle:Device:add.html.twig',['form'=>$form->createView(),]);
    }

}
