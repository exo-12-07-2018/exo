<?php

namespace Perso\MyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('PersoMyBundle:Default:index.html.twig');
    }
}
