<?php

namespace Perso\MyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Perso\MyBundle\Entity\Bridge;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

use Symfony\Component\Form\Extension\Core\Type\DateType;

use Symfony\Component\Form\Extension\Core\Type\FormType;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use Symfony\Component\Form\Extension\Core\Type\TextType;

class BridgeController extends Controller
{
    /**
     * @Route("/index")
     */
    public function indexAction()
    {


        



        //$form = $this->createForm(DeviceType::class, $device);
        $em = $this->getDoctrine()->getManager();
        $listAllBridges= $em->getRepository('PersoMyBundle:Bridge')->findAll();
        return $this->render('PersoMyBundle:Bridge:index.html.twig', array(
            // ...
            
            'allbridges'=>$listAllBridges

        ));
        
    }

    /**
     * @Route("/add")
     */
    public function addAction(Request $request)
    {


        $bridge = new Bridge();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $bridge);

        $formBuilder

          ->add('name',      TextType::class)
          ->add('guid',     TextType::class)
          ->add('b_range',   IntegerType::class)
          ->add('protocol',   TextType::class)

          
        ;

        

        $form = $formBuilder->getForm();
         if($request->isMethod('POST')){
            $data=$form->getData();
            $bridge->setName($data['name']);
            $bridge->setGuid($data['guid']);
            $bridge->setBRange($data['b_range']);
            $bridge->setProtocol($data['protocol']);

            $request->getSession()->getFlashBag()->add('tag','les actions ont été save');
            return $this->redirectToRoute('perso_my_bridges');
        }
        return $this->render('PersoMyBundle:Bridge:add.html.twig',['form'=>$form->createView(),]);
    }

}
